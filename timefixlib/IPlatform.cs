﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TimefixLib
{
    public interface IPlatform
    {
        Dictionary<int, byte[]> GetImageTimeProps(string fileFullName, out Exception exception);
        string SetImageTimeProps(string fileFullName, Dictionary<int, byte[]> props, DateTime time, out Exception exception);

        void ShowCount(int current, int max);
        void AddList(string str);

        string SetFileTimeProps(string fileInfoFullName, DateTime time, out Exception exception);
    }
}
