﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;


namespace TimefixLib
{
    public class TimeFix
    {
        const string match = @"\d{8}_\d{6}";
        const string dateFormat = "yyyyMMddHHmmss";
        const string dateFormat2 = "yyyyMMdd_HHmmss";
        const string unicmathc = @"^\d{10,13}";
        const string unicmathc2 = @"^IMG\d{14}";
        const string shoottimemathc = "yyyy:MM:dd HH:mm:ss";
        const string shootmatch = @"\d{4}:\d{2}:\d{2} \d{2}:\d{2}:\d{2}";
        const string splitChar = @"[^0-9]{0,1}"; //@"\W{0,1}";
        const string dateMatch = @"[1-2]\d{3}" + splitChar + @"(0[1-9]|1[0-2])" + splitChar + @"(0[1-9]|[1-2][0-9]|3[0-1])";
        const string timematch = @"(20|21|22|23|[0-1]\d)" + splitChar + @"[0-5]\d" + splitChar + @"[0-5]\d";

        static readonly string[] imageExtensions = { ".jpg", ".png", ".gif" };
        static readonly string[] videoExtensions = { ".3gp", ".mp4", ".avi" };
        private double allTicksToNow = 0;
        IPlatform platform;
        Queue<Action> invokes = new Queue<Action>();
        private List<Thread> thred = new List<Thread>();

        private int totalcount;
        private int currentcount;

        private Action doneAction;

        private List<FileInfo> _fileInfos;
        private TimeFix()
        {

        }
        public TimeFix(IPlatform platform)
        {
            this.platform = platform;
        }
        public void Run(List<FileInfo> fileInfos, Action callback)
        {
            if (thred.Count > 0)
            {
                throw new DuplicateWaitObjectException("程序已在运行，请勿重复运行");
            }
            allTicksToNow = (DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds;
            totalcount = fileInfos.Count;
            doneAction = callback;
            _fileInfos = fileInfos;
            ConcurrentQueue<FileInfo> quene = new ConcurrentQueue<FileInfo>(fileInfos);
            lock (invokes) invokes.Clear();

            //thred.Add(new Thread(() => RunFiles(quene)));
            //thred.Add(new Thread(() => RunFiles(quene)));
            //thred.Add(new Thread(() => RunFiles(quene)));
            thred.Add(new Thread(() => RunFiles(quene)));

            for (int i = 0; i < thred.Count; i++)
            {
                thred[i].Start();
            }
            while (true)
            {
                Update();

                Thread.Sleep(1);
                bool finish = true;
                if (thred.Count > 0)
                {
                    for (int i = 0; i < thred.Count; i++)
                    {
                        finish &= !thred[i].IsAlive;
                    }
                    if (finish || fileInfos.Count <= 0)
                    {
                        //Thread.Sleep(1000);
                        Done();
                        thred.Clear();
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
        }
        public List<FileInfo> GetList()
        {
            return _fileInfos;
        }
        private void Update()
        {
            lock (invokes)
            {
                while (invokes.Count > 0)
                {

                    invokes.Dequeue()();
                }
            }
            platform.ShowCount(currentcount, totalcount);

        }
        public void Run(DirectoryInfo directory, Action callback)
        {
            List<FileInfo> fileInfos = new List<FileInfo>();
            foreach (FileInfo fileInfo in directory.GetFiles())
            {
                if (imageExtensions.Contains(fileInfo.Extension.ToLower()) || videoExtensions.Contains(fileInfo.Extension.ToLower()))
                {
                    fileInfos.Add(fileInfo);
                }
            }


            Run(fileInfos, callback);
        }
        private void RunFiles(ConcurrentQueue<FileInfo> fileInfos)
        {
            Thread.Sleep(thred.Count * 2);
            //lock (fileInfos)
            {
                while (fileInfos.Count > 0)
                {
                    FileInfo fileInfo;
                    lock (fileInfos)
                    {
                        while (!fileInfos.TryDequeue(out fileInfo))
                        {
                            if (fileInfos.Count == 0)
                            {
                                return;
                            }
                        }
                    }
                    InvokeAddList("正在处理：" + fileInfo.Name);
                    bool error = false;

                    if (fileInfo.Length < 1)
                    {
                        InvokeAddList("错误：图片损坏=>" + fileInfo.Name);
                        error = true;
                    }
                    if (!error)
                    {
                        DateTime time = default(DateTime);
                        Match dateMatchResult = Regex.Match(fileInfo.Name, dateMatch);
                        Match timeMatchResult = Regex.Match(fileInfo.Name, timematch);
                        Match datetimeMatchResult = Regex.Match(fileInfo.Name, dateMatch + splitChar + timematch);

                        double t;
                        if (double.TryParse(Path.GetFileNameWithoutExtension(fileInfo.Name), out t))
                        {
                            DateTime dt = new DateTime(1970, 1, 1).AddMilliseconds(t);

                            if (dt < DateTime.Now && dt > new DateTime(2013, 1, 1))
                            {
                                time = dt;
                            }
                            else
                            {
                                dt = new DateTime(1970, 1, 1).AddSeconds(t);
                                if (dt < DateTime.Now && dt > new DateTime(2013, 1, 1))
                                {
                                    time = dt;
                                }
                            }
                        }
                        if (datetimeMatchResult.Success)
                        {
                            long pticks = 0;
                            bool bdate = false;
                            if (long.TryParse(datetimeMatchResult.Value, out pticks))
                            {
                                if (pticks > 0 && pticks < allTicksToNow)
                                {
                                    time = GetTime(pticks);
                                }
                                else
                                {
                                    bdate = true;
                                }
                            }
                            else
                            {
                                bdate = true;
                            }
                            if (bdate)
                            {
                                string num = Regex.Replace(datetimeMatchResult.Value, "[^0-9]", "", RegexOptions.IgnoreCase);

                                time = DateTime.ParseExact(num, dateFormat, CultureInfo.CurrentCulture);
                            }
                        }

                        if (false)
                        {
                            if (Regex.IsMatch(fileInfo.Name, match))

                            {
                                var timeselect = Regex.Match(fileInfo.Name, match).Value;
                                time = DateTime.ParseExact(timeselect, dateFormat2, CultureInfo.CurrentCulture);
                            }
                            else if (Regex.IsMatch(fileInfo.Name, unicmathc))
                            {
                                var timeselect = Regex.Match(fileInfo.Name, unicmathc).Value;
                                if (!string.IsNullOrEmpty(timeselect))
                                {
                                    time = GetTime(long.Parse(timeselect));
                                }
                            }

                            else if (Regex.IsMatch(fileInfo.Name, unicmathc2))
                            {
                                var timeselect = Regex.Match(fileInfo.Name, @"\d{14}").Value;
                                time = DateTime.ParseExact(timeselect, dateFormat, CultureInfo.CurrentCulture);
                            }
                        }

                        if (!(time > default(DateTime)))
                        {
                            InvokeAddList("错误：未能识别时间=>" + fileInfo.Name);
                            error = true;
                        }
                        // Console.WriteLine(fileInfo.Name + @"," + time.ToString(CultureInfo.CurrentCulture));

                        if (!error)
                        {
                            if (imageExtensions.Contains(fileInfo.Extension.ToLower()))
                            {

                                try
                                {
                                    Exception e = null;
                                    Dictionary<int, byte[]> dic = platform.GetImageTimeProps(fileInfo.FullName, out e);
                                    if (e != null)
                                    {
                                        throw e;
                                    }
                                    Dictionary<int, byte[]> dic2 = new Dictionary<int, byte[]>();

                                    bool changed = false;
                                    byte[] times = new byte[0];
                                    if (ShootTime(time, ref times, true))
                                    {
                                        if (!dic.ContainsKey(29))
                                        {
                                            dic.Add(29,times);
                                        }
                                        if (!dic.ContainsKey(306))
                                        {
                                            dic.Add(306, times);
                                        }
                                    }
                                    //306 29 36867 36868
                                    try
                                    {
                                        foreach (var keyValuePair in dic)
                                        {

                                            bool force = keyValuePair.Key == 29 || keyValuePair.Key == 306 || keyValuePair.Key == 36867 || keyValuePair.Key == 36868;
                                            if (force)
                                            {
                                                dic2.Add(keyValuePair.Key, times); ;
                                                changed = true;
                                            }

                                        }
                                        if (!dic2.ContainsKey(29))
                                        {
                                            dic2.Add(29, times);
                                        }
                                        if (!dic2.ContainsKey(306))
                                        {
                                            dic2.Add(306, times);
                                        }
                                        if (!dic2.ContainsKey(36867))
                                        {
                                            dic2.Add(36867, times);
                                        }
                                        if (!dic2.ContainsKey(36868))
                                        {
                                            dic2.Add(36868, times);
                                        }
                                    }
                                    catch (Exception exception)
                                    {
                                        e = exception;
                                    }
                                    FileInfo newfie = fileInfo;
                                    if (changed)
                                    {
                                        //成功
                                        string path = platform.SetImageTimeProps(fileInfo.FullName, dic2, time, out e);
                                        if (!string.IsNullOrEmpty(path))
                                        {
                                            newfie = new FileInfo(path);
                                        }
                                    }
                                    platform.SetFileTimeProps(newfie.FullName, time, out e);
                                    if (e != null)
                                    {
                                        throw e;
                                    }
                                }
                                catch (Exception exception)
                                {
                                    Console.WriteLine(exception);
                                    if (exception is OutOfMemoryException)
                                    {
                                        InvokeAddList("错误：图片损坏=>" + fileInfo.Name);
                                    }
                                    else if (exception is ExternalException)
                                    {
                                        InvokeAddList("错误：目录权限不足" + fileInfo.Name);
                                    }
                                    else
                                    {
                                        InvokeAddList("错误：" + fileInfo.Name + "=>" + exception);
                                    }

                                    error = true;
                                }
                                finally
                                {
                                    GC.Collect();
                                }
                            }
                            else if (videoExtensions.Contains(fileInfo.Extension.ToLower()))
                            {
                                Exception e = null;
                                platform.SetFileTimeProps(fileInfo.FullName, time, out e);
                            }

                        }
                    }
                    if (!error)
                    {
                        currentcount++;
                    }
                }
            }
            Thread.Sleep(1000);

        }



        private void Invoke(Action action)
        {
            lock (invokes) invokes.Enqueue(action);
        }
        private bool ShootTime(DateTime time, ref byte[] p, bool force = false)
        {
            bool changed = false;


            string shotdate = Encoding.ASCII.GetString(p);
            shotdate = shotdate.Replace("\0", "");
            shotdate = shotdate.Replace(" 24", " 00");
            if (!Regex.IsMatch(shotdate, shootmatch))
            {
                if (!force)
                {
                    return false;
                }
            }
            try
            {
                //"2013:08:28 11:47:21\0"
                DateTime shootTime;
                bool isDate = DateTime.TryParseExact(shotdate, shoottimemathc, CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces, out shootTime);
                if ((isDate && (time - shootTime).Duration() > TimeSpan.FromDays(1)) || force)
                {
                    byte[] newBytes = Encoding.ASCII.GetBytes(time.ToString(shoottimemathc));
                    if (newBytes[newBytes.Length - 1] != (byte)'\0')
                    {
                        Array.Resize(ref newBytes, newBytes.Length + 1);
                        newBytes[newBytes.Length - 1] = (byte)'\0';
                    }
                    string log = Encoding.ASCII.GetString(p) + " => " + Encoding.ASCII.GetString(newBytes);
                    log.Replace("\0", "");
                    Console.WriteLine(log);
                    p = newBytes;
                    changed = true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

            return changed;
        }
        private void InvokeShowCount(int current, int max)
        {
            Invoke(() => platform.ShowCount(current, max));
        }

        private void InvokeAddList(string str)
        {
            Console.WriteLine("列表消息：" + str);
            Invoke(() => platform.AddList(str));
        }

        public static DateTime GetTime(long timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan toNow = new TimeSpan();
            if (timeStamp > int.MaxValue)
            {
                toNow = TimeSpan.FromMilliseconds(timeStamp);
            }
            else
            {
                toNow = TimeSpan.FromTicks(timeStamp);
            }
            return dtStart.Add(toNow);
        }

        private void Done()
        {
            if (doneAction != null)
            {
                doneAction();
            }
        }

        public void Stop()
        {
            foreach (Thread thread in thred)
            {
                if (thread != null)
                {
                    thread.Abort();
                }
            }
            thred.Clear();
            totalcount = 0;
            currentcount = 0;
        }
    }
}
