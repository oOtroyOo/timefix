﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace timefix
{
    static class Program
    {
        public static Form from1;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            from1= new Form1();
            Application.Run(from1);
        }
    }
}
