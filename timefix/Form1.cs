﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Threading.Tasks;
using TimefixLib;

namespace timefix
{
    public partial class Form1 : Form, TimefixLib.IPlatform
    {




        private DirectoryInfo directory;
        private DirectoryInfo newDirectoryInfo;
        private DirectoryInfo oldDirectoryInfo;
        private TimeFix timefix;

        private string folderpath
        {
            get { return textBox1.Text; }
            set
            {
                if (Directory.Exists(value))
                {
                    textBox1.Text = value;
                }
            }
        }
        public Form1()
        {
            InitializeComponent();

            timefix = new TimeFix(this);

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            buttonStop.Hide();
            buttonRun.Show();
            this.listBox1.SelectedIndexChanged += this.listBox1_SelectedIndexChanged;
        }

        void Done()
        {
            MessageBox.Show("已完成");
            Stop();
            //this.listBox1.SelectedIndexChanged += listBox1_SelectedIndexChanged;
        }
        private void buttonSelect_Click(object sender, EventArgs e)
        {
            DialogResult res = folderBrowserDialog1.ShowDialog();

            if (res == DialogResult.OK)
            {
                folderpath = folderBrowserDialog1.SelectedPath;
            }
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(folderpath))
                return;
            buttonStop.Show();
            buttonRun.Hide();
            listBox1.Items.Clear();
            directory = new DirectoryInfo(folderpath);
            newDirectoryInfo = directory.CreateSubdirectory("转换");
            oldDirectoryInfo = directory.CreateSubdirectory("已完成的旧文件");
            //List<FileInfo> fileInfos = new List<FileInfo>();
            //fileInfos.AddRange(directory.GetFiles("*.jpg"));
            //fileInfos.AddRange(directory.GetFiles("*.png"));
            //fileInfos.AddRange(directory.GetFiles("*.bmp"));
            //timefix.Run(fileInfos, Done);


            timefix.Run(directory, Done);
            this.listBox1.SelectedIndexChanged -= this.listBox1_SelectedIndexChanged;


        }

        private Dictionary<string, Image> images = new Dictionary<string, Image>();
        public PropertyItem[] GetExifProperties(string fileName, bool dispose = true)
        {
            System.Drawing.Image image = null;
            //通过指定的数据流来创建Image
            if (images.ContainsKey(fileName))
            {
                image = images[fileName];
            }
            else
            {

                try
                {
                    image = Image.FromFile(fileName);

                }
                catch (Exception e)
                {
                    image?.Dispose();
                    throw e;
                }

            }
            PropertyItem[] pro;
            try
            {
                pro = image.PropertyItems;
            }
            catch (Exception e)
            {
                image?.Dispose();
                throw e;
            }

            if (dispose)
            {
                if (images.ContainsKey(fileName))
                {
                    images.Remove(fileName);
                }
                image.Dispose();
            }
            else
            {
                images.Add(fileName, image);
            }
            return pro;
        }

        private string GetTakePicDateTime(System.Drawing.Imaging.PropertyItem[] parr)
        {
            Encoding ascii = Encoding.ASCII;
            //遍历图像文件元数据，检索所有属性
            foreach (System.Drawing.Imaging.PropertyItem p in parr)
            {
                //如果是PropertyTagDateTime，则返回该属性所对应的值
                if (p.Id == 0x0132)
                {
                    return ascii.GetString(p.Value);
                }
            }
            //若没有相关的EXIF信息则返回N/A
            return "N/A";
        }
        public static DateTime GetTime(long timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            TimeSpan toNow = new TimeSpan();
            if (timeStamp > int.MaxValue)
            {
                toNow = TimeSpan.FromMilliseconds(timeStamp);
            }
            else
            {
                toNow = TimeSpan.FromTicks(timeStamp);
            }
            return dtStart.Add(toNow);
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {

            Stop();
        }

        void Stop()
        {

            timefix.Stop();
            buttonRun.Show();
            buttonStop.Hide();

        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            timefix.Stop();
        }

        public Dictionary<int, byte[]> GetImageTimeProps(string fileFullName, out Exception exception)
        {
            Dictionary<int, byte[]> dic = new Dictionary<int, byte[]>();
            exception = null;
            try
            {
                PropertyItem[] props = GetExifProperties(fileFullName);
                foreach (PropertyItem p in props)
                {
                    dic.Add(p.Id, p.Value);
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            return dic;
        }
        public string SetFileTimeProps(string fileFullName, DateTime time, out Exception exception)
        {
            FileInfo newFileInfo = null;
            FileInfo oldFile = null;
            exception = null;
            newFileInfo = new FileInfo(Path.Combine(newDirectoryInfo.FullName, Path.GetFileName(fileFullName)));
            //oldFile = new FileInfo(Path.Combine(oldDirectoryInfo.FullName, Path.GetFileName(fileFullName)));
            try
            {
                if (!File.Exists(newFileInfo.FullName))
                {
                    File.Copy(fileFullName, newFileInfo.FullName);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                exception = e;
                return null;
            }
            try
            {
                if (File.Exists(newFileInfo.FullName))
                {
                    if (time > default(DateTime))
                    {
                        newFileInfo.LastWriteTime = time;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                exception = e;
                return null;
            }
           
            return newFileInfo.FullName;
        }
        public string SetImageTimeProps(string fileFullName, Dictionary<int, byte[]> props, DateTime time, out Exception exception)
        {
            Image image = null;
            exception = null;
            FileInfo newFileInfo = null;
            FileInfo oldFile = null;
            try
            {
                newFileInfo = new FileInfo(Path.Combine(newDirectoryInfo.FullName, Path.GetFileName(fileFullName)));
                oldFile = new FileInfo(Path.Combine(oldDirectoryInfo.FullName, Path.GetFileName(fileFullName)));
                //通过指定的数据流来创建Image
                if (images.ContainsKey(fileFullName))
                {
                    image = images[fileFullName];
                }
                else
                {

                    image = Image.FromFile(fileFullName);
                }
                PropertyItem p = null;
                //306 29 36867 36868
                foreach (PropertyItem propertyItem in image.PropertyItems)
                {
                    if (propertyItem.Id == 306 || propertyItem.Id == 29 || propertyItem.Id == 36867 || propertyItem.Id == 36868)
                    {
                        p = propertyItem;
                    }
                }
                if (p == null)
                {
                    throw new ArgumentException();
                }
          
                foreach (KeyValuePair<int, byte[]> prop in props)
                {
                    p.Id = prop.Key;
                    p.Value = prop.Value;
                    image.SetPropertyItem(p);
                }
                if (File.Exists(newFileInfo.FullName))
                {
                    newFileInfo.Delete();
                }
                image.Save(newFileInfo.FullName);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                exception = e;
                return null;
            }
            finally
            {
                if (image != null)
                {
                    image.Dispose();
                }
                GC.Collect();
            }
            if (File.Exists(fileFullName))
            {
                File.Move(fileFullName, oldFile.FullName);
            }
            return newFileInfo.FullName;
        }

        public void ShowCount(int current, int max)
        {
            progressBar1.Value = Math.Min(progressBar1.Maximum, current);
            labelCurrent.Text = current.ToString();

            progressBar1.Maximum = max;
            labelTotal.Text = max.ToString();
        }

        public void AddList(string str)
        {
            listBox1.Items.Add(str);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
        }



        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();
            if (listBox1.Items.Count > 0)
            {
                ////让文字位于Item的中间
                //float difH = (e.Bounds.Height - e.Font.Height) / 2;
                //RectangleF rf = new RectangleF(e.Bounds.X, e.Bounds.Y + difH, e.Bounds.Width, e.Font.Height);
                //e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), rf);

                Color color = Color.Black;

                if (listBox1.Items[e.Index].ToString().StartsWith("错误"))
                {
                    color = Color.Red;

                }
                e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font, new SolidBrush(color), e.Bounds);
            }
        }


        private void listBox1_MeasureItem(object sender, MeasureItemEventArgs e)
        {


            e.ItemHeight = 30;
            //if (e.Index == 2)//只设置第三项的高度
            //{
            //    e.ItemHeight = 50;
            //}
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //Console.WriteLine(timefix.GetList()[listBox1.SelectedIndex].FullName);
        }

    }
}
