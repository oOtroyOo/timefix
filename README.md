# timefix

打开项目之前，请运行这条命令，确保行尾不会被意外自动转换<br />
<br />
git config --global core.autocrlf false<br />
<br />
需要先安装 Xamarin.VisualStudio 与 VisualStudio （2015及以下）<br />
如果已安装Xamarin.VisualStudio，或者你使用的是VisualStudio 2017 则跳过安装步骤<br />
<br />
VisualStudio 下载地址。。自己找，很多<br />
<br />
在安装VisualStudio 时，选择Xamarin 安装。<br />
或者手动下载安装Xamarin for Visual Studio<br />
[https://developer.xamarin.com/releases/vs/](https://developer.xamarin.com/releases/vs/)
<br />
<br />
先不要打开本项目，参考搜索这些文章将环境配置好：<br />
[Xamarin.Forms 环境搭建、创建项目](https://www.baidu.com/s?wd=Xamarin.Forms%20%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E3%80%81%E5%88%9B%E5%BB%BA%E9%A1%B9%E7%9B%AE)<br />
<br />
<br />
打开项目，点击 生成-重写生成解决方案<br />
生成结束后，关闭解决方案，再次打开<br />
应该就可以用了<br />