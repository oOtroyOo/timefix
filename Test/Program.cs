﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Test
{
    class Program
    {
        const string ultTimeMathc = @"^[1-9]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\s+((20|21|22|23|[0-1]\d):[0-5]\d:[0-5]\d){0,1}$";
        static void Main(string[] args)
        {
            string input = "";
            input = "2017-01-01 11:25:22";
            var match = Regex.Match(input, ultTimeMathc);
            if (match.Success)
            {
                Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("失败");
            }
            Console.ReadLine();
        }
    }
}
